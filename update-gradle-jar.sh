#!/bin/sh

JAR=$(find build/libs -name "micrograph-*.jar" -type f)
JAR_NAME=`basename $JAR`
[ ! -z "$JAR_NAME" ] && find $HOME/.gradle -name "$JAR_NAME" -type f -exec cp $JAR {} \;
