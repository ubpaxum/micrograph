package org.ibro.micrograph.processor;

import groovy.lang.GroovyShell;
import org.ibro.micrograph.processor.common.Task;
import org.ibro.micrograph.processor.def.ProcessDef;
import org.ibro.micrograph.processor.def.ProcessGraph;

import java.util.Map;

/**
 * Simple mini processor
 */
public interface Process<T,V> {

    String VAR_APP_NAME     = "appName";
    String VAR_INPUT        = "input";
    String VAR_RESULT       = "result";
    String VAR_PROCESS_ID   = "processId";
    String VAR_PROCESS      = "process";
    String VAR_CURR_TASK    = "currTask";

    /**
     * Sets process definition graph
     * @param processGraph
     */
    void setProcessGraph(ProcessGraph processGraph);

    /**
     * Returns process definition graph
     * @return  processGraph
     */
    ProcessGraph getProcessGraph();

    /**
     * Sets application name
     * @param appName
     */
    void setAppName(String appName);

    /**
     * Returns application name
     * @return
     */
    String getAppName();
    /**
     * Invoked when before processing is started - initialize needed resources here
     */
    void onEntry() throws Exception;

    /**
     * Do actual processing and returns the result
     * @param in
     * @return
     */
    T process(V in);

    /**
     * Ivnoked after processing is finished - release all resources here
     */
    void onExit() throws Exception;
    /**
     * Returns name of the process
     * @return
     */
    String getName();

    /**
     * Returns unique processId
     * @return
     */
    String getProcessId();

    /**
     * Returns process definition
     * @return
     */
    ProcessDef getProcessDef();

    /**
     * Returns process variable value
     * @param name
     * @param <X>
     * @return
     */
    <X> X getVariable(String name);

    <X> void setVariable(String name, X value);

    /**
     * Invoked when runtime exception is raised during processing
     * @param t
     */
    void onException(Throwable t);

    /**
     * Returns the exception if process was aborted
     * @return exception or null
     */
    Throwable getException();

    /**
     * Returns the current/last task run by process
     * @return
     */
    Task getCurrentTask();

    /**
     * Returns if process is aborted (i.e. in case of processing exception)
     * @return true if process is aborted
     */
    boolean isAborted();

    void graphProcess(ProcessGraph graph) throws Exception;

    /**
     * Returns execution shell
     * @return
     */
    GroovyShell getShell();

    /**
     * Set groovy shell
     */
    void setShell(GroovyShell shell);

    /**
     * Stop execution of process
     */
    void abortProcess();

    /**
     * Exit process (including sub-processes)
     */
    void exitProcess();

    /**
     * Return
     * @return true if all processes shall exit immediately
     */
    boolean isExited();

    /**
     * Returns factory for task creation
     * @return factory instance for task creation
     */
    Factory getFactory();

    /**
     * Set process parameters
     * @param parameters
     */
    void setParameters(Map<String,Object> parameters);

    /**
     * Returns process parameters
     * @return process parameters map
     */
    Map<String,Object> getParameters();
}
