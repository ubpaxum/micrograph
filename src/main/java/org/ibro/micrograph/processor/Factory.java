package org.ibro.micrograph.processor;

import com.paypal.digraph.parser.GraphNode;
import org.ibro.micrograph.processor.common.Task;
import org.ibro.micrograph.processor.def.ProcessGraph;
import org.ibro.micrograph.processor.def.TaskDef;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public class Factory{
    private ProcessConfig config;
    private String appName;
    private Router router;
    private static Factory mInstance;

    private Factory(String appName) throws ProcessException {
        this.appName = appName;
        this.config = Processor.getInstance(appName).getConfig();
        this.router = createRouter();
    }

    public static final Factory getInstance(String appName) throws ProcessException{
        if ( mInstance == null)
            mInstance = new Factory(appName);
        return mInstance;
    }

    private  Router createRouter() throws ProcessException {
        Class<Router> routerClass  = toClass(config.getDefaultRouterClass());
        Router router = newInstance( routerClass );
        router.setAppName( appName );
        router.setImports( config.getImports() );
        router.setRoutes( config.getRoutes() );
        return router;
    }

    public Router getRouter(){
        return this.router;
    }

    public <T extends Process> T createProcess(String processName) throws ProcessException {
        ProcessGraph graph = createProcessGraph(processName);
        if ( graph.getProcessDef().getClazz() == null ){
            graph.getProcessDef().setClazz( toClass(graph.getProcessDef().getProcessClass()) );
        }
        T process = newInstance( (Class<T>) graph.getProcessDef().getClazz());
        process.setAppName(appName);
        process.setProcessGraph( graph );
        try {
            setProcessProperties(process, graph.getProcessDef().getProcessParams() );
        } catch (Exception e) {
            throw new ProcessException("Cannot set custom properties of process class "+graph.getProcessDef().getProcessClass());
        }
        return process;
    }

    public ProcessGraph createProcessGraph(String processName) throws ProcessException {
        return Processor.getInstance(appName).getProcessGraph(processName);
    }

    public Task createTask(Process process, GraphNode taskNode) throws  ProcessException {
        String taskType = toTaskType(taskNode);
        Task task = createTask(process,taskType);
        task.setName(taskNode.getId());
        return task;
    }

    public Task createTask(Process process, String taskType) throws  ProcessException {
        TaskDef def = config.getTasks().get(taskType);
        if ( def == null )
            throw new ProcessException("Task "+taskType+" not defined!");
        if ( def.getClazz() == null ){
            def.setClazz( toClass(def.getTaskClass()) );
        }
        Task task = newInstance( def.getClazz() );
        task.setProcess(process);
        task.setTaskDef(def);
        return task;
    }

    public static <T> T newInstance(Class<T> clazz) throws ProcessException{
        try {
            return (T) clazz.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            throw new ProcessException("Class "+clazz.getName()+" with no default constructor!",e);
        }
    }

    public static <T> Class<T> toClass(String className) throws ProcessException{
        try {
            Class clazz =Thread.currentThread().getContextClassLoader().loadClass(className);
            return clazz;
        } catch (ClassNotFoundException e) {
            throw new ProcessException("Class "+className+"  not found!",e);
        }
    }

    public static String toRealTaskName(String taskName){
        String taskLower = taskName.toLowerCase();
        if ( taskLower.equals("start") ){
            return  "Start";
        } else if ( taskLower.startsWith("end") ){
            return "End";
        } else if ( taskLower.startsWith("gateway") ){
            return "Gateway";
        } else if ( taskLower.startsWith("script") ){
            return "Script";
        } else if ( taskLower.startsWith("graphroute") ){
            return "GraphRoute";
        } else if ( taskLower.startsWith("split") ){
            return "Split";
        } else if ( taskLower.startsWith("join") ){
            return "Join";
        } else if ( taskLower.startsWith("exit") ){
            return "Exit";
        }
        return taskName;
    }

    public boolean validate() throws ProcessException{
        if ( config.isValidateOnStartup() ){
            boolean isValid = Processor.getInstance(appName).validate();
            if (!isValid)
                throw new ProcessException("Processor configuration is not valid. See log for details.");
        }
        return true;
    }

    public static String toTaskType(GraphNode taskNode){
        String taskType = (String) taskNode.getAttribute("type");
        if ( taskType == null ) return toRealTaskName(taskNode.getId());
        return toRealTaskName( taskType );
    }

    private void setProcessProperties(Process p, Map<String,Object> properties) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if ( properties == null ) return;
        for ( String prop: properties.keySet() ){
            Object val= properties.get(prop);
            if ( val == null ) continue;
            String methodName = "set"+Character.toTitleCase(prop.charAt(0))+prop.substring(1);
            Method  setMethod = p.getClass().getMethod(methodName, val.getClass() );
            setMethod.invoke(p,val);
        }
    }

}
