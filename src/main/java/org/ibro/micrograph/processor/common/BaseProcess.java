package org.ibro.micrograph.processor.common;

import com.paypal.digraph.parser.GraphEdge;
import com.paypal.digraph.parser.GraphNode;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.ibro.micrograph.processor.Factory;
import org.ibro.micrograph.processor.Process;
import org.ibro.micrograph.processor.ProcessException;
import org.ibro.micrograph.processor.def.ProcessDef;
import org.ibro.micrograph.processor.def.ProcessGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseProcess<T,V> implements Process<T,V> {
    protected static final Logger log = LoggerFactory.getLogger(BaseProcess.class);
    private static final char[] CHARACTER_SET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    private static final int UUID_LEN  = 16;
    private GroovyShell shell;
    private Throwable   exception;
    private Factory     factory;


    /* protected - only for internal use by implementations */
    protected Task currentTask;
    protected boolean aborted;
    /** Indicator if process shall end immediately (from subgraph) */
    protected boolean exited;

    protected String appName;
    protected String processId;
    protected ProcessGraph processGraph;
    protected Map<String,Object> parameters;

    public BaseProcess() {
    }

    @Override
    public void setProcessGraph(ProcessGraph processGraph) {
        this.processGraph = processGraph;
    }

    @Override
    public ProcessGraph getProcessGraph() {
        return processGraph;
    }

    @Override
    public void setAppName(String appName) {
        this.appName=appName;
    }

    @Override
    public String getAppName() {
        return appName;
    }

    @Override
    public void onEntry() throws Exception {
        log.trace("onEntry: {}", getName());
        this.factory = Factory.getInstance(appName);
        Binding binding = shell.getContext();
        this.processId = generateUuid(UUID_LEN);
        binding.setProperty(VAR_APP_NAME,appName);
        binding.setProperty(VAR_PROCESS_ID,processId);
        binding.setProperty(VAR_PROCESS, this);
        this.aborted = false;
        this.exited = false;
    }

    @Override
    public abstract T process(V in);

    @Override
    public void onExit() throws Exception {
        log.trace("onExit: {}", getName());
    }

    @Override
    public String getName() {
        return processGraph.getProcessDef().getName();
    }

    @Override
    public String getProcessId() {
        return processId;
    }

    @Override
    public ProcessDef getProcessDef() {
        return processGraph.getProcessDef();
    }

    @Override
    public <X> X getVariable(String name) {
        if ( shell.getContext().hasVariable(name) ) {
            return (X) shell.getContext().getVariable(name);
        }
        return null;
    }

    @Override
    public <X> void setVariable(String name, X value) {
        if ( shell == null  ) shell = new GroovyShell(new Binding());
        shell.getContext().setVariable(name, value);
    }

    @Override
    public void onException(Throwable t){
        log.error("Exception in process",t);
        this.exception = t;
        this.aborted = true;
    }

    @Override
    public Task getCurrentTask() {
        return currentTask;
    }

    @Override
    public boolean isAborted() {
        return aborted;
    }

    @Override
    public Throwable getException() {
        return exception;
    }

    /* ==== Helper protected method ===== */
    protected void taskExecute(Task task) throws Exception {
        this.currentTask = task;
        setVariable(Process.VAR_CURR_TASK,currentTask);
        if ( task.isShallExecute() ){
            if (!aborted) task.onEntry();
            if (!aborted) task.execute();
            if (!aborted) task.onExit();
        }
    }

    @Override
    public void graphProcess(ProcessGraph graph) throws Exception {
        List<GraphEdge> startList =  graph.getEdges("Start");
        if ( startList == null || startList.size() != 1)
            throw new ProcessException("No 'Start' or more than one 'Start' in graph file "+getProcessDef().getGraphFile());

        GraphNode start = graph.getEdge("Start").getNode1();
        graphExec(graph,start);
    }

    protected GraphNode graphExec(final ProcessGraph graph, final GraphNode start) throws Exception {
        GraphNode current = start;
        GraphNode lastProcessed = null;
        while (current!=null && !aborted && !exited){
            if ( current == lastProcessed )
                throw new ProcessException("Dead-loop-node detected "+current);
            lastProcessed = current;
            Task task = factory.createTask(this,current);
            if ( task instanceof EndTask || task instanceof ExitTask ){
                log.debug("Executing {}", task);
                taskExecute(task);
                return null;
            }
            if ( task instanceof JoinTask ){
                return current;
            }
            this.currentTask = task;
            setVariable(Process.VAR_CURR_TASK,currentTask);
            task.setParameters( parseParams(graph, current.getId(), current.getAttributes()));
            if ( !aborted && task.isShallExecute() ) {
                log.debug("Executing {}", task);
                task.onEntry();
            }
            for ( GraphEdge curr: graph.getEdges(current) ) {
                task.setParameters( parseParams(graph, curr.getId(), curr.getAttributes()) );
                if ( task instanceof GatewayTask &&
                     curr.getNode2().getAttribute("if") != null ){
                    task.setParameter( Task.ATTR_IF, parseParameter(graph, curr.getId(), Task.ATTR_IF, curr.getNode2().getAttribute(Task.ATTR_IF)) );
                }
                if ( !aborted && task.isShallExecute() ) task.execute();
                if ( task instanceof GatewayTask ) {
                    GatewayTask gwTask = (GatewayTask) task;
                    if (gwTask.isShallSplit()) {
                        current = curr.getNode2();
                        break;
                    }
                } else if ( task instanceof SplitTask ) {
                    GraphNode joinNode = null;
                    for ( GraphEdge e: graph.getEdges( current ) ){
                        joinNode=graphExec(graph, e.getNode2() );
                    }
                    if ( joinNode != null) {
                        Task joinTask = factory.createTask(this,joinNode);
                        log.debug("Executing {}", joinTask);
                        taskExecute(joinTask);
                        current = graph.getEdge(joinNode).getNode2();
                        break;
                    }
                } else {
                    current= curr.getNode2();
                    break;
                }
            }
            if ( !aborted && task.isShallExecute() ) task.onExit();
        }
        return null;
    }


    /**
     * Generates an alphanumeric string based on specified length.
     * @param length of characters to generate
     * @return random string
     */
    protected String generateUuid(int length) {
        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();
        int  factor = CHARACTER_SET.length;
        long time = new Date().getTime();
        while ( time > 0 ){
            Long remainder = time % factor;
            time = time/factor;
            sb.append( CHARACTER_SET[remainder.intValue()] );
        }
        sb.reverse();
        for (int i = sb.length(); i < length; i++) {
            int randomCharIndex = random.nextInt(factor);
            sb.append( CHARACTER_SET[randomCharIndex] );
        }
        return sb.toString();
    }

    @Override
    public GroovyShell getShell(){
        return shell;
    }

    @Override
    public void setShell(GroovyShell shell) {
        this.shell = shell;
    }

    @Override
    public void abortProcess() {
        log.info("Abort process {}", getCurrentTask());
        this.aborted = true;
    }

    @Override
    public void exitProcess() {
        log.info("Exit process {}", getCurrentTask());
        this.exited = true;
    }

    @Override
    public boolean isExited() {
        return exited;
    }

    @Override
    public String toString() {
        return getName()+"@"+getProcessId();
    }

    protected Map<String,Object> parseParams(ProcessGraph graph, String nodeId, Map<String,Object> in){
        Map<String,Object> out = new HashMap<>();
        for(String key: in.keySet()){
            Object val = in.get(key);
            out.put(key, parseParameter(graph,nodeId,key,val));
        }
        return out;
    }

    protected Object parseParameter(ProcessGraph graph, String nodeId, String key, Object val){
        Script graphVal = graph.getScript(nodeId,key);
        if ( Task.EXPR_ATTRIBUTES.contains(key)  ){
            val =  graphVal == null?parseExpression(graph,nodeId,key,val.toString()):graphVal;
        } else if ( (val instanceof String) && ( ((String)val).startsWith("${") ) ){
            if ( graphVal == null ){
                String expr = ((String) val).substring(2);
                expr = expr.substring(0,expr.length()-1);
                val=parseExpression(graph,nodeId,key,expr);
            } else {
                val = graphVal;
            }
        }
        return val;
    }

    private Script parseExpression(ProcessGraph graph, String nodeId, String key, String expression){
//        long startTime = System.currentTimeMillis();
        Script script = getShell().parse(expression);
        graph.addScript(nodeId,key,script);
//        log.debug("Parse: graph: {}, key: {}@{} expr: {}, script: {}, elapsed: {} ms", graph.getName(), nodeId, key, expression, script, System.currentTimeMillis()-startTime);
        return script;
    }

    @Override
    public Factory getFactory() {
        return factory;
    }

    @Override
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    @Override
    public Map<String, Object> getParameters() {
        return parameters;
    }
}
