package org.ibro.micrograph.processor.common;

import groovy.lang.GroovyShell;
import org.ibro.micrograph.processor.Factory;
import org.ibro.micrograph.processor.Process;
import org.ibro.micrograph.processor.ProcessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultRouter<T,V> extends BaseRouter<T,V>{
    protected static final Logger log = LoggerFactory.getLogger(DefaultRouter.class);
    private Object lock = new Object();

    @Override
    public T route(V in) throws ProcessException {
        String processName;
        synchronized (lock) {
            routingScript.getBinding().setVariable(Process.VAR_INPUT, in);
            processName = (String) routingScript.run();
        }
        return routeTo(processName,in);
    }

    @Override
    public T routeTo(String processName, V in) throws ProcessException {
        if ( processName == null )
            throw new ProcessException("No route found for input ("+(in==null?"null":in.toString())+").");
        long start = System.currentTimeMillis();
        GroovyShell shell = createShell();
        shell.setVariable(Process.VAR_INPUT, in);
        log.info("Routing {} to {}",in, processName);
        Process<T,V> process = Factory.getInstance(appName).createProcess(processName);
        process.setShell(shell);
        T res =process.process(in);
        log.info("Process {}({}) elapsed {} ms", process, in, System.currentTimeMillis()-start);
        return res;
    }

}
