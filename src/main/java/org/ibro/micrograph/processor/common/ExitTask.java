package org.ibro.micrograph.processor.common;

public class ExitTask extends BaseTask{
    @Override
    public void execute() {
        getProcess().exitProcess();
    }
}
