package org.ibro.micrograph.processor.common;

import org.ibro.micrograph.processor.Process;
import org.ibro.micrograph.processor.ProcessException;
import org.ibro.micrograph.processor.def.ProcessGraph;

import java.util.Map;

/**
 * Route process to another graph
 */
public class GraphRouteTask extends BaseTask {

    @Override
    public void execute() throws Exception {
        Map<String,Object> params = getParameters();
        String processName = (String) params.get("process");
        if ( processName == null)
            throw new ProcessException("Process  for routing not specified!");
        Process process = getProcess();
        ProcessGraph graph = process.getFactory().createProcessGraph(processName);
        ProcessGraph saveGraph = process.getProcessGraph();
        Map<String, Object> saveParams = process.getParameters();
        process.setProcessGraph( graph );
        process.setParameters(params);
        log.debug("route to {} -> {} {}", saveGraph.getName(), graph.getName(), params );
        process.graphProcess(graph);
        log.debug("return from {} -> {}", graph.getName(), saveGraph.getName() );
        process.setProcessGraph(saveGraph);
        process.setParameters(saveParams);
    }
}
