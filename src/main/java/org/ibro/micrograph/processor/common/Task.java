package org.ibro.micrograph.processor.common;

import org.ibro.micrograph.processor.Process;
import org.ibro.micrograph.processor.def.TaskDef;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public interface Task extends NamedItem {

    String ATTR_WHEN = "when";
    String ATTR_EXPR = "expr";
    String ATTR_IF   = "if";
    String ATTR_TYPE = "type";

    List<String> EXPR_ATTRIBUTES = Arrays.asList(ATTR_IF,ATTR_WHEN,ATTR_EXPR);
    List<String> DEFFER_EVAL_ATTRIBUTES = Arrays.asList(ATTR_IF,ATTR_EXPR);

    void  setProcess(Process p);

    void setTaskDef(TaskDef d);
    /**
     * Returns value of task input
     * @return
     */
    Object getInput(String name);

    /**
     * Sets parameters of the task
     * @param parameters - parameter map
     */
    public void setParameters(Map<String,Object> parameters);

    void setParameter(String key, Object val);

    Map<String,Object> getParameters();

    /**
     * Returns value of task parameter
     * @param name of the parameter
     * @return null if parameter not set
     */
    <X extends Object> X getParameter(String name);

    /**
     * Returns value of task output
     * @return
     */
    Object getOutput(String name);

    /**
     * Sets output
     * @param name
     * @param val
     */
    void setOutput(String name, Object val);

    /**
     * Executes the task
     */
    void execute() throws Exception;

    /**
     * Returns the process instance
     * @return
     */
    <X extends Process> X getProcess();

    /**
     * Returns the name of the task
     * @return
     */
    String getName();

    /**
     * Returns the task definition
     * @return
     */
    TaskDef getTaskDef();

    /**
     * Executed before task is executed
     */
    void onEntry() throws Exception;

    /**
     * Executed after task is executed
     */
    void onExit() throws Exception;

    /**
     * returns process variable value
     */
    <X extends Object> X getVariable(String name);

    /**
     * sets process variable value
     */
    void setVariable(String name, Object val);

    boolean isShallExecute();
}
