package org.ibro.micrograph.processor.common;

public interface NamedItem {
    /**
     * Returns name of the task
     * @return
     */
    String getName();

    /**
     * Returns task description
     * @return
     */
    String getDescription();

    /**
     * Sets name of the item
     * @param name
     */
    void setName(String name);
}
