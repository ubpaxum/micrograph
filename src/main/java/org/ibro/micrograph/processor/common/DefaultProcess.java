package org.ibro.micrograph.processor.common;

public class DefaultProcess<T,V> extends BaseProcess<T,V> {
    public DefaultProcess() {
        super();
    }

    @Override
    public T process(V in) {
        try {
            setVariable(VAR_INPUT, in);
            onEntry();
            graphProcess( processGraph );
            onExit();
        } catch (Throwable e){
            this.onException(e);
        }
        T result = getVariable(VAR_RESULT);
        return result;
    }

}
