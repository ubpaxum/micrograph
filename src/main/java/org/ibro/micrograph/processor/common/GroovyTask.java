package org.ibro.micrograph.processor.common;

public abstract class GroovyTask extends BaseTask{
    @Override
    public abstract void execute() throws Exception;
}
