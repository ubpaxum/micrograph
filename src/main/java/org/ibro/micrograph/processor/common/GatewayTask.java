package org.ibro.micrograph.processor.common;

import groovy.lang.Script;
import org.ibro.micrograph.processor.ProcessException;


public class GatewayTask extends GroovyTask {
    private boolean shallSplit;
    @Override
    public void execute() throws Exception {
        Script res = getParameter(ATTR_IF);
        if ( res == null )
            throw new ProcessException("No 'if' expression defined for gateway "+this.toString());
        this.shallSplit = asBoolean( evaluate(res) );
    }

    public boolean isShallSplit() {
        return shallSplit;
    }
}
