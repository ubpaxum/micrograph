package org.ibro.micrograph.processor.common;

import groovy.lang.Binding;
import groovy.lang.Script;
import org.ibro.micrograph.processor.Process;
import org.ibro.micrograph.processor.def.TaskDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseTask implements Task {
    protected Logger log ;
    private Process process;
    private TaskDef taskDef;
    private String name;
    private Map<String,Object> parameters;
    private boolean shallExecute = true;

    public BaseTask() {
        log = LoggerFactory.getLogger(this.getClass());
        this.parameters = new HashMap<>();
    }

    public BaseTask(Process process, TaskDef taskDef) {
        this();
        this.process = process;
        this.taskDef = taskDef;
    }

    @Override
    public void setProcess(Process process) {
        this.process = process;
    }

    @Override
    public void setTaskDef(TaskDef taskDef) {
        this.taskDef = taskDef;
    }

    @Override
    public Object getInput(String name) {
        //TODO:
        return null;
    }

    @Override
    public void setParameters(Map<String,Object> params) {
        params.forEach((k,v) -> setParameter(k,v));
    }

    @Override
    public void setParameter(String key,Object val) {
        Object res = val;
        if ( val != null && (val instanceof Script) ){
            Script expr = (Script) val;
            if ( key.equals(ATTR_WHEN) ){
                res = evaluate(expr );
                this.shallExecute = asBoolean(res);
            } else if ( DEFFER_EVAL_ATTRIBUTES.contains(key) ){
                // do not execute deffered attributes - they will be executed later
            } else {
                res = evaluate( (Script) val );
            }
        }
        parameters.put(key,res);
    }

    @Override
    public Map<String,Object> getParameters(){
        return this.parameters;
    }

    @Override
    public <X extends Object> X getParameter(String name) {
        return (X)parameters.get(name);
    }

    @Override
    public Object getOutput(String name) {
        //TODO:
        return null;
    }

    @Override
    public void setOutput(String name, Object val) {
        //TODO:
    }

    @Override
    public abstract void execute() throws Exception;

    @Override
    public <X extends Process> X getProcess() {
        return (X) process;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name==null?taskDef.getName():name;
    }

    @Override
    public TaskDef getTaskDef() {
        return taskDef;
    }

    @Override
    public String getDescription() {
        return taskDef.getDescription();
    }

    @Override
    public void onEntry() throws Exception{
    }

    @Override
    public void onExit() throws Exception{
    }

    @Override
    public <X> X getVariable(String name) {
        return (X) getProcess().getVariable(name);
    }

    @Override
    public void setVariable(String name, Object val) {
        getProcess().setVariable(name, val);
    }

    public Object evaluate(Script expression){
        if ( expression == null )
            throw new RuntimeException("Expression to evaluate is null");
        long startTime = System.currentTimeMillis();
        Object res;
        synchronized (expression){
            Binding binding = getProcess().getShell().getContext();
            expression.setBinding( binding );
            res= expression.run();
        }
        log.trace("Evaluate {} elapsed {} ms", expression, System.currentTimeMillis()-startTime );
        return res;
    }

    @Override
    public boolean isShallExecute() {
        return shallExecute;
    }

    @Override
    public String toString() {
        if ( process == null ) return super.toString();
        return process.getName()+":"+getName()+"@"+process.getProcessId();
    }

    protected boolean asBoolean(Object v){
        return v != null &&
                ( ( v instanceof Boolean && ((Boolean)v).booleanValue() ) ||
                        ( v instanceof String && "true".equals(v) ) ||
                        ( v instanceof List && !((List)v).isEmpty() ) ||
                        ( v instanceof Number && ((Number)v).intValue()!=0 ) );
    }

}
