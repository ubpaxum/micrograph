package org.ibro.micrograph.processor.common;

import groovy.lang.GroovyShell;
import groovy.lang.Script;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ImportCustomizer;
import org.ibro.micrograph.processor.ProcessException;
import org.ibro.micrograph.processor.Router;
import org.ibro.micrograph.processor.def.RouteDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public abstract class BaseRouter<T,V>  implements Router<T,V> {
    private static final Logger log = LoggerFactory.getLogger(DefaultRouter.class);
    protected List<RouteDef> routes;
    protected Script routingScript;
    protected String appName;
    protected List<String> imports;

    public BaseRouter() {
    }

    @Override
    public void setAppName(String appName) {
        this.appName = appName;
    }

    @Override
    public String getAppName() {
        return appName;
    }

    @Override
    public abstract T route(V in) throws ProcessException;

    @Override
    public abstract T routeTo(String processName,V in) throws ProcessException;

    @Override
    public void setRoutes(List<RouteDef> routes) {
        this.routes = routes;
        GroovyShell shell = createShell();
        StringBuilder routerSrc = new StringBuilder();
        routerSrc.append("def route(input){\n");
        String defaultRoute=null;
        for ( RouteDef r: routes ){
            if (r.getWhen()!=null) routerSrc.append("if ("+r.getWhen()+") return '"+r.getTo()+"'\n" );
            else defaultRoute = "return '"+r.getTo()+"'\n";
        }
        if (defaultRoute!=null) routerSrc.append(defaultRoute);
        routerSrc.append("}\nroute(input)\n");
        if ( log.isTraceEnabled() ) log.trace("Routing script:\n{}\n",routerSrc.toString());
        routingScript=shell.parse(routerSrc.toString());
    }

    @Override
    public void setImports(List<String> imports) {
        this.imports = imports;
    }

    protected boolean asBoolean(Object v){
        return v != null &&
                ( ( v instanceof Boolean && ((Boolean)v).booleanValue() ) ||
                        ( v instanceof List && !((List)v).isEmpty() ) ||
                        ( v instanceof Number && ((Number)v).intValue()!=0 ) );
    }

    protected GroovyShell createShell(){
        long start = System.currentTimeMillis();
        ImportCustomizer customizer = new ImportCustomizer();
        if ( imports != null ){
            customizer.addImports( imports.toArray(new String[]{}) );
        }
        CompilerConfiguration configuration = new CompilerConfiguration();
        configuration.addCompilationCustomizers(customizer);
        GroovyShell shell= new GroovyShell( configuration );
        log.trace("Groovy shell created for {} ms", System.currentTimeMillis() - start);
        return shell;
    }

}
