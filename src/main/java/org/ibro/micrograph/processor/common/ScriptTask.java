package org.ibro.micrograph.processor.common;


import groovy.lang.Script;

public class ScriptTask extends GroovyTask {
    private Object result;
    @Override
    public void execute() throws Exception {
        Script expr = getParameter(ATTR_EXPR);
        result = evaluate(expr);
    }

    public Object getResult() {
        return result;
    }
}
