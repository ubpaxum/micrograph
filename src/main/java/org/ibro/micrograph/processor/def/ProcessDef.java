package org.ibro.micrograph.processor.def;

import org.ibro.micrograph.processor.Process;

import java.util.Map;

public class ProcessDef extends NamedDef{
    private String processClass;
    private Class<? extends Process> clazz;
    private String graphFile;
    private Map<String,Object> processParams;

    public ProcessDef() {
    }

    public String getProcessClass() {
        return processClass;
    }

    public void setProcessClass(String processClass) {
        this.processClass = processClass;
    }

    public String getGraphFile() {
        return graphFile;
    }

    public void setGraphFile(String graphFile) {
        this.graphFile = graphFile;
    }

    public Class<? extends Process> getClazz() {
        return clazz;
    }

    public void setClazz(Class<? extends Process> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toString() {
        return "ProcessDef{" +
                "name='" + getName() + '\'' +
                "processClass='" + processClass + '\'' +
                "graphFile='" + graphFile + '\'' +
                '}';
    }

    public Map<String, Object> getProcessParams() {
        return processParams;
    }

    public void setProcessParams(Map<String, Object> processParams) {
        this.processParams = processParams;
    }
}
