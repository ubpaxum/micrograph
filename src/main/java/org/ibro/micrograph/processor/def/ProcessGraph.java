package org.ibro.micrograph.processor.def;

import com.paypal.digraph.parser.GraphEdge;
import com.paypal.digraph.parser.GraphNode;
import com.paypal.digraph.parser.GraphParser;
import groovy.lang.Script;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Container for parsed graph for specific process
 */
public class ProcessGraph {
    private ProcessDef processDef;
    protected Map<String,GraphNode> graphNodes;
    protected Map<String,GraphEdge> graphEdges;
    protected Map<String,List<GraphEdge>> graphPaths;
    protected Map<String,Script> groovyScript;

    public ProcessGraph(ProcessDef processDef) {
        this.processDef = processDef;
        GraphParser parser = new GraphParser(  ProcessGraph.class.getResourceAsStream(processDef.getGraphFile()) );
        this.graphNodes = parser.getNodes();
        this.graphEdges = parser.getEdges();
        this.groovyScript = new HashMap<>();
        // create paths - NodeName -> List<GraphEdges> grouped by NodeName (id)
        this.graphPaths = graphEdges.
                values()
                .stream()
                .collect(Collectors.groupingBy(e -> e.getNode1().getId() ));
    }

    public String getName(){
        return processDef.getName();
    }

    public ProcessDef getProcessDef() {
        return processDef;
    }

    public List<GraphEdge> getEdges(GraphNode node){
        return getEdges(node.getId());
    }

    public List<GraphEdge> getEdges(String nodeId){
        List<GraphEdge> paths = graphPaths.get(nodeId);
        if ( paths == null ) return Collections.EMPTY_LIST;
        return paths;
    }

    public GraphEdge getEdge(GraphNode node){
        return getEdge(node.getId());
    }

    public GraphEdge getEdge(String nodeId){
        List<GraphEdge> paths = getEdges(nodeId);
        if ( paths.isEmpty() ) return null;
        return paths.get(0);
    }

    public Map<String, GraphNode> getGraphNodes() {
        return graphNodes;
    }

    public void addScript(String nodeId, String attName, Script script){
        groovyScript.put( nodeId+"@"+attName, script );
    }

    public Script getScript(String nodeId, String attName){
        return groovyScript.get(nodeId+"@"+attName);
    }
}
