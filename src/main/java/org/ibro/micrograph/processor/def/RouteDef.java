package org.ibro.micrograph.processor.def;

public class RouteDef extends NamedDef {
    private String when;
    private String to;

    public RouteDef() {
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "RouteDef{" +
                "when='" + when + '\'' +
                ", to='" + to + '\'' +
                '}';
    }
}
