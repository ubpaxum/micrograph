package org.ibro.micrograph.processor.def;

import org.ibro.micrograph.processor.common.Task;

import java.util.List;

public class TaskDef extends NamedDef {
    private String taskClass;
    private List<String> input;
    private List<String> output;
    private Class<Task> clazz;

    public TaskDef() {
    }

    public TaskDef(String name, String taskClass) {
        this();
        this.taskClass = taskClass;
        setName(name);
        setDescription(name);
    }

    public String getTaskClass() {
        return taskClass;
    }

    public void setTaskClass(String taskClass) {
        this.taskClass = taskClass;
    }

    public List<String> getInput() {
        return input;
    }

    public void setInput(List<String> input) {
        this.input = input;
    }

    public List<String> getOutput() {
        return output;
    }

    public void setOutput(List<String> output) {
        this.output = output;
    }

    public Class<Task> getClazz() {
        return clazz;
    }

    public void setClazz(Class<Task> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String toString() {
        return "TaskDef{" +
                "name='" + getName()+ '\'' +
                "taskClass='" + taskClass + '\'' +
                ", input=" + input +
                ", output=" + output +
                '}';
    }
}
