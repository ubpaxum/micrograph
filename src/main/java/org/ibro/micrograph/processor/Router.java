package org.ibro.micrograph.processor;

import org.ibro.micrograph.processor.def.RouteDef;

import java.util.List;

/**
 * Simple mini router
 */
public interface Router<T,V> {
    /**
     * Sets application name
     * @param appName
     */
    void setAppName(String appName);

    /**
     * Returns application name
     * @return
     */
    String getAppName();

    /**
     * Do actual processing and returns the result
     * @param in
     * @return
     */
    T route(V in) throws ProcessException;

    /**
     * Routes to specific process
     * @param processName
     * @param in
     * @return
     * @throws ProcessException
     */
    T routeTo(String processName, V in) throws ProcessException;

    void setRoutes(List<RouteDef> routes);

    /**
     * Set import list
     * @param imports
     */
    void setImports(List<String> imports);
}
