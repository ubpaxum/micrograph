package org.ibro.micrograph.processor;

import com.paypal.digraph.parser.GraphEdge;
import org.ibro.micrograph.processor.common.Task;
import org.ibro.micrograph.processor.def.ProcessDef;
import org.ibro.micrograph.processor.def.ProcessGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class Processor {
    private static final Logger log = LoggerFactory.getLogger(Processor.class);
    private static Processor mInstance;
    private String appName;
    private ProcessConfig config;
    private Map<String,ProcessGraph> graphCache;
    private Object lock = new Object();

    private Processor(String appName){
        this.appName = appName;
        try {
            this.config = ProcessConfig.loadConfiguration("/"+appName+"-processor.yaml");
            log.debug("Processor config: {}", config);
            graphCache = new HashMap<>();
        } catch (Exception e) {
            log.error("Cannot load processor configuration",e);
        }
    }

    public static Processor getInstance(String appName){
        if (mInstance == null) {
            mInstance = new Processor(appName);
        }
        return mInstance;
    }

    public ProcessConfig getConfig() {
        return config;
    }

    public ProcessGraph getProcessGraph(String processName) throws ProcessException {
        ProcessGraph graph = graphCache.get(processName);
        if ( graph == null ){
            synchronized (lock){
                ProcessDef processDef = config.getProcesses().get(processName);
                if ( processDef ==null )
                    throw new ProcessException("No process defined with name: "+processName);
                graph = new ProcessGraph(processDef);
                graphCache.put(processName,graph);
            }
        }
        return graph;
    }

    /**
     * Checks processor configuration
     *  * if all graph files are present and syntax of DOT files is valid
     *  * if all described taskClasses are present
     * @return true if valid, false if invalid
     */
    public boolean validate() throws ProcessException {
        boolean isValid = true;
        Factory factory = Factory.getInstance(appName);
        long startTime = System.currentTimeMillis();
        try {
            log.debug("Validating router {}", config.getDefaultRouterClass());
            Router router = Factory.getInstance(appName).getRouter();
            log.debug("Router {} - {} -OK", config.getDefaultRouterClass(), router.getClass().getName());
        } catch (Throwable e){
            log.error("Router {} -NOK. Reason: {}",config.getDefaultRouterClass(), e.getMessage());
            isValid = false;
        }
        for (String processName: config.getProcesses().keySet()){
            try {
                log.debug("Validating process {}...",processName);
                Process<Object,Object> process = factory.createProcess(processName);
                ProcessGraph graph = process.getProcessGraph();
                GraphEdge start = graph.getEdge("Start");
                if ( start == null )
                    throw new ProcessException("Start node not defined in graph");
                for ( String node: graph.getGraphNodes().keySet() ){
                    log.debug("Checking node {} ...", node);
                    Task task = factory.createTask(process, graph.getGraphNodes().get(node) );
                    log.debug("Node {} - {} - OK", node, task.getClass().getName());
                }
                log.info("Process {} - {} - OK",processName,process.getClass().getName());
            } catch (Throwable e) {
                log.error("Process {} -NOK. Reason: {}",processName, e.getMessage());
                isValid=false;
            }
        }
        log.info("Processor validation: {}, elapsed: {} ms", isValid, (System.currentTimeMillis()-startTime) );
        return isValid;
    }
}
