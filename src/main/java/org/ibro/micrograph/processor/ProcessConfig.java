package org.ibro.micrograph.processor;

import org.ibro.micrograph.processor.def.ProcessDef;
import org.ibro.micrograph.processor.def.RouteDef;
import org.ibro.micrograph.processor.def.TaskDef;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessConfig {
    private static final String DEFAULT_CONFIG = "/micrograph-defaults.yaml";
    private String defaultProcessPackage;
    private String defaultTaskPackage;
    private String defaultProcessClass;
    private String defaultRouterClass;
    private boolean validateOnStartup;

    private Map<String,TaskDef> tasks;

    private Map<String,ProcessDef> processes;
    private List<RouteDef> routes;
    private List<String> imports;
    private List<String> includes;

    public ProcessConfig() {
        tasks = new HashMap<>();
        processes = new HashMap<>();
        imports = new ArrayList<>();
        includes = new ArrayList<>();
        routes = new ArrayList<>();
    }

    public String getDefaultProcessPackage() {
        return defaultProcessPackage;
    }

    public void setDefaultProcessPackage(String defaultProcessPackage) {
        this.defaultProcessPackage = defaultProcessPackage;
    }

    public String getDefaultTaskPackage() {
        return defaultTaskPackage;
    }

    public void setDefaultTaskPackage(String defaultTaskPackage) {
        this.defaultTaskPackage = defaultTaskPackage;
    }

    public String getDefaultProcessClass() {
        return defaultProcessClass;
    }

    public void setDefaultProcessClass(String defaultProcessClass) {
        this.defaultProcessClass = defaultProcessClass;
    }

    public Map<String, TaskDef> getTasks() {
        return tasks;
    }

    public void setTasks(Map<String, TaskDef> tasks) {
        this.tasks = tasks;
    }

    public Map<String, ProcessDef> getProcesses() {
        return processes;
    }

    public void setProcesses(Map<String, ProcessDef> processes) {
        this.processes = processes;
    }

    public String getDefaultRouterClass() {
        return defaultRouterClass;
    }

    public void setDefaultRouterClass(String defaultRouterClass) {
        this.defaultRouterClass = defaultRouterClass;
    }

    public List<RouteDef> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RouteDef> routes) {
        this.routes = routes;
    }

    public List<String> getIncludes() {
        return includes;
    }

    public void setIncludes(List<String> includes) {
        this.includes = includes;
    }

    public void applyDefaults(){
        processes.forEach((k,v)->{
            v.setName(k);
            if (v.getProcessClass()==null) v.setProcessClass(defaultProcessClass);
        });
        tasks.forEach((k,v) -> {
            v.setName(k);
            if( v.getTaskClass().indexOf('.')<0 ) v.setTaskClass(defaultTaskPackage+"."+v.getTaskClass());
        } );
    }

    public void applyIncludes() throws Exception {
        if ( getIncludes().isEmpty()  ) {
            ProcessConfig defaultConfig = loadDefault();
            mergeInclude(defaultConfig);
            return;
        }

        for ( String path: getIncludes() ){
            ProcessConfig config1 = loadConfiguration(path);
            mergeInclude(config1);
        }
    }

    private void mergeInclude(ProcessConfig from){
        if ( getDefaultTaskPackage() == null ) setDefaultRouterClass(from.getDefaultTaskPackage());
        if ( getDefaultRouterClass() == null ) setDefaultRouterClass(from.getDefaultRouterClass());
        if ( getDefaultProcessClass() == null ) setDefaultProcessClass(from.getDefaultProcessClass());
        if ( getDefaultProcessPackage() == null ) setDefaultProcessPackage(from.getDefaultProcessPackage());
        from.getProcesses().forEach( (k,v) -> { if ( !getProcesses().containsKey(k) ) getProcesses().put(k,v); } );
        from.getTasks().forEach( (k,v) -> { if ( !getTasks().containsKey(k) ) getTasks().put(k,v); } );
        getRoutes().addAll( from.getRoutes() );
        getImports().addAll( from.getImports() );
    }

    public static ProcessConfig loadConfiguration(String path) throws Exception{
        try (InputStream is = ProcessConfig.class.getResourceAsStream(path) ) {
            if ( is == null ) {
                throw new ProcessException("Configuration "+path+" not found as resource.");
            }
            ProcessConfig config = new Yaml().loadAs(is, ProcessConfig.class);
            config.applyIncludes();
            config.applyDefaults();
            return config;
        }
    }

    public static ProcessConfig loadDefault() throws Exception{
        try (InputStream is = ProcessConfig.class.getResourceAsStream(DEFAULT_CONFIG) ) {
            if ( is == null ) {
                throw new ProcessException("Configuration "+DEFAULT_CONFIG+" not found as resource.");
            }
            ProcessConfig config = new Yaml().loadAs(is, ProcessConfig.class);
            config.applyDefaults();
            return config;
        }
    }

    public List<String> getImports() {
        return imports;
    }

    public void setImports(List<String> imports) {
        this.imports = imports;
    }

    public boolean isValidateOnStartup() {
        return validateOnStartup;
    }

    public void setValidateOnStartup(boolean validateOnStartup) {
        this.validateOnStartup = validateOnStartup;
    }

    @Override
    public String toString() {
        return "ProcessConfig{" +
                "defaultProcessPackage='" + defaultProcessPackage + '\'' +
                ", defaultTaskPackage='" + defaultTaskPackage + '\'' +
                ", defaultProcessClass='" + defaultProcessClass + '\'' +
                ", defaultRouterClass='" + defaultRouterClass + '\'' +
                ", tasks=" + tasks +
                ", processes=" + processes +
                ", routes=" + routes +
                '}';
    }
}
