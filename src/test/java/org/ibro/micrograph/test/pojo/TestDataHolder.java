package org.ibro.micrograph.test.pojo;

public class TestDataHolder {
    private Integer id;
    private String name;

    public TestDataHolder(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TestDataHolder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
