package org.ibro.micrograph.test;

import com.paypal.digraph.parser.GraphEdge;
import com.paypal.digraph.parser.GraphNode;
import com.paypal.digraph.parser.GraphParser;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GraphTest {

    @Test
    public void testGraph() throws Exception{
        GraphParser parser = new GraphParser( GraphParser.class.getResourceAsStream("/graph/test.gv") );
        parser.getNodes().
                forEach((key,node) -> {
                    System.out.printf("%s: %s: attr: %s\n",key, node.getId(), node.getAttributes());
                });

        System.out.printf("\nEdges\n");
        for (GraphEdge edge : parser.getEdges().values()) {
            System.out.printf("%s: %s -> %s\n", edge.getId(), edge.getNode1(), edge.getNode2());
        }

        System.out.printf("\nExecution\n");

        Collection<GraphEdge> edges = parser.getEdges().values();
        GraphEdge curr = edges.stream().filter(e-> e.getNode1().getId().equals("Start") ).findFirst().orElse(null);
        graphExec(edges,curr);

        Map<String,List<GraphEdge>> paths =
            edges.stream()
                .collect(
                        Collectors.groupingBy( e -> e.getNode1().getId() )
                );

        System.out.printf("paths: %s", paths);
    }

    private void graphExec(final Collection<GraphEdge> edges, GraphEdge curr){
        System.out.printf("%s\n",curr.getNode1());
        final GraphNode next = curr.getNode2();
        edges.stream()
                .filter(e-> e.getNode1().getId().equals(next.getId()))
                .forEach( e-> graphExec(edges,e) );
    }
}
