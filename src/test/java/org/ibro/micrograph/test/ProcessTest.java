package org.ibro.micrograph.test;

import org.ibro.micrograph.processor.*;
import org.ibro.micrograph.processor.Process;
import org.ibro.micrograph.test.pojo.TestDataHolder;
import org.ibro.micrograph.test.process.CustomProcess;
import org.junit.Test;

public class ProcessTest {
    private static final String appName = "test";

    @Test
    public void testProcessorConfig() throws Exception {
        ProcessConfig config = Processor.getInstance(appName).getConfig();
        assert config!=null;
        System.out.printf("\n%s\n",config);
    }

    @Test
    public void testProcessorSuccess() throws ProcessException {
        Factory factory = Factory.getInstance(appName);
        Process<String,TestDataHolder> proc = factory.createProcess("GuaranteeCheck");
        String res = proc.process(  new TestDataHolder(1,"one") );
        assert res != null;
        assert "SUCCESS".equals(res);
    }


    @Test
    public void testProcessorFail() throws ProcessException {
        Factory factory = Factory.getInstance(appName);
        Process<String,TestDataHolder> proc = factory.createProcess("GuaranteeCheck");
        String res = proc.process( new TestDataHolder(0,"zero") );
        assert res != null;
        assert "ERROR".equals(res);
    }

    @Test
    public void testCustomProcessorSuccess() throws ProcessException {
        Factory factory = Factory.getInstance(appName);
        CustomProcess<String,TestDataHolder> proc = factory.createProcess("CustomGuaranteeCheck");
        String res = proc.process( new TestDataHolder(0,"zero") );
        assert res != null;
        assert "ERROR".equals(res);
    }

    @Test
    public void testRouterSuccess() throws ProcessException {
        Factory factory = Factory.getInstance(appName);
        Router<String,TestDataHolder> router = factory.getRouter();
        String res = router.route(new TestDataHolder(1,"one"));
        assert res != null;
        assert "SUCCESS".equals(res);
    }

    @Test
    public void testRouter2Success() throws ProcessException {
        Factory factory = Factory.getInstance(appName);
        Router<String,TestDataHolder> router = factory.getRouter();
        String res = router.route(new TestDataHolder(0,"zero"));
        assert res != null;
        assert "ERROR".equals(res);
    }

    @Test
    public void testGraphRoute() throws ProcessException {
        Factory factory = Factory.getInstance(appName);
        Process<String,TestDataHolder> proc = factory.createProcess("TestGraphRoute");
        String res = proc.process(  new TestDataHolder(1,"one") );
        assert res != null;
        assert "SUCCESS".equals(res);
    }

    @Test
    public void testGraphRouteWithExit() throws ProcessException {
        Factory factory = Factory.getInstance(appName);
        Process<String,TestDataHolder> proc = factory.createProcess("TestGraphRouteWithExit");
        String res = proc.process(  new TestDataHolder(-1,"minus one") );
        assert res != null;
        assert "SUCCESS".equals(res);
    }

    @Test
    public void testValidate() throws ProcessException{
        Processor processor = Processor.getInstance(appName);
        assert processor.validate() == false;
    }

    @Test
    public void testDeadGatewayGraph() throws ProcessException {
        Factory factory = Factory.getInstance(appName);
        Process<String,TestDataHolder> proc = factory.createProcess("DeadGatewayGraph");
        String res = proc.process(new TestDataHolder(0, "zero"));
        assert proc.isAborted();
        assert proc.getException() instanceof ProcessException;
        assert proc.getException().getMessage().startsWith("Dead-loop-node detected GraphNode-GatewayGueCheck");
    }

}
