package org.ibro.micrograph.test.task;

import org.ibro.micrograph.processor.common.DefaultProcess;
import org.ibro.micrograph.test.pojo.TestDataHolder;
import org.slf4j.LoggerFactory;

public class MisvEadRead extends LoggerTask{
    @Override
    public void execute() {
        super.execute();
        DefaultProcess proc = getProcess();
        TestDataHolder data = getVariable("input");
        LoggerFactory.getLogger(MisvEadRead.class).debug("data input: {}", data);
    }
}
