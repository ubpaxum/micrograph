package org.ibro.micrograph.test.task;

import org.ibro.micrograph.processor.common.BaseTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerTask extends BaseTask {
    private Logger log = LoggerFactory.getLogger(LoggerTask.class);
    @Override
    public void execute() {
        log.info("Execute {}:{}, params: {}", getProcess().getName(), getName(), getParameters() );
    }
}
