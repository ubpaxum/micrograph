package org.ibro.micrograph.test.process;

import org.ibro.micrograph.processor.common.DefaultProcess;

public class CustomProcess<T,V> extends DefaultProcess<T,V> {
    private boolean doLocks = false;
    public CustomProcess() {
        super();
    }

    public boolean isDoLocks() {
        return doLocks;
    }

    public void setDoLocks(Boolean doLocks) {
        this.doLocks = doLocks;
    }
}
