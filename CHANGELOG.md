# Release Notes for micrograph

## v0.7.2 (25/03/2021)
#### Enhancements:
- Detect dead-loop - usually when all gateway edges evaluate to false.

## v0.7.1 (29/01/2021)
#### Enhancements:
- support process parameters for subgraphs 
- create initial documentaiton in README.md  

## v0.7.0 (26/01/2021)
#### Enhancements:
- add Exit tasks - exist gracefully from process

## v0.6.0 (23/07/2019)
#### Enhancements:
- implement includes - to add common configurations (tasks, process, etc.)
