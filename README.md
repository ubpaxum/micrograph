Micrograph
=============
*Micrograph* is Java light engine, alternative of BPMN execution modules (like jBPMN). 
It executes processes with tasks described in *DOT* language - https://graphviz.org/doc/info/lang.html.

# Main definitions
 * **Task** - task is the atomic step, executed by micrograph engine. It is described by name and *TaskClass*. All task shall implement *Task* interface (or extend *BaseTask*)
 * **Process** - process is a graph definition described in DOT, executed by the engine. 
   Process is defined as *oriented graph*, where each *node* is *Task* and every *edge* 
   is transition to following node. Transition can be conditional in case if *gateway* task.
 * **Path** - this is the path (set of ordered tasks) in which process is executed.
 * **Router** - this is main entry point of micrograph. It evaluates conditions defined 
   application configuration (by YAML) and routes the processing to specific graph. 

# Main concepts
## Types of nodes
 * Start - there shall be one-and-only-one start node in the graph. 
   This is the node from which the execution of process starts.
 * End - end nodes is where process finishes. There can be many end nodes in one graph.
 * base task - task executed by the engine. All shall implement Task (or extend BaseTask)   
 * Gateway - this is a conditional node, which splits process to different paths.
 * GraphRoute - this node routes to subgraph. The instance of process is not changed. 
   When subgraph is ended, the process returns to calling graph.  
 * Exit - this is like end node, but will exit the process if called from subgraph.
 * Script - this task executes expression specified in the *expr* attribute of the node. 
   Expression is executed in groovy script language.
 * Split - this is junction to start execution of consecutive tasks. 
 * Join - this is joining node of split

## Routing
Every process starts with input. The routing logic is based on input. Input can be any Java object. 

Routing logic is described in configuration file (in YAML) and will decide to which graph processing will be executed.

## Configuration
Micrograph load configuration from YAML file named **<appliction-name>-processor.yaml**. Main sections are:
 * defaultProcessPackage - default java package of processor classes. One can extend *BaseProcess* class to implement onw processor. 
 * defaultTaskPackage - default java package of task (nodes). All tasks shall implement *Task* class
 * defaultProcessClass - name of default process class. If not specified - *DefaultProcess*
 * includes - in this section can be used to import definitions in other configuration files. Multiple can be specified and are loaded as resources from JVM, so paths shall start "/".
 * imports - in this section list of import (Java/groovy import statements) can be specified to support easy (and short) references to user-defined classes.
 * tasks - in this section custom task are described as name, description and taskClass. Every custom node used in graph shall be defined.
 * processes - in this section all graph definitions shall be defined with name, description and graphFile 
 * routes - in this section routing to graph shall be defined for the router.

### Sample configuration
```yaml
defaultProcessPackage: org.ibro.micrograph.test.task
defaultTaskPackage: org.ibro.micrograph.test.task
defaultProcessClass: org.ibro.micrograph.processor.common.DefaultProcess
defaultRouterClass: org.ibro.micrograph.processor.common.DefaultRouter
imports:
  - org.ibro.micrograph.test.pojo.TestDataHolder
  - org.ibro.micrograph.test.pojo.OtherData

includes:
  - /graph/common-tasks.yaml

tasks:
  CreatePojoMessageResult:
    description: Creates result PojoMessageBean
    taskClass: CreatePojoMessageBean

  CreateRulesErrorDoc:
    description: Create RulesErrorDoc based on rules error
    taskClass: CreateRulesErrorDoc

processes:
  GuaranteeCheck:
    description: Checks validity of guarantee for EAD
    graphFile: /graph/test.gv

  ExitGraph:
    description: Exit grpah
    graphFile: /graph/exit.gv

routes:
  - to: GuaranteeCheck
    when: "input instanceof TestDataHolder && input.id > 0"
  - to: ExitGraph
    when: "input instanceof TestDataHolder && input.id == 0"
``` 

## Process initialization
Micrograph engine has singleton router, but processes are run in different java threads. In simple form initialization is like this:
```java
public class Init{
    public void initGraph(){
        MyCustomObject input = new MyCustomObject();
        Factory factory = Factory.getInstance("myapp");
        Router<MyCustomObject,MyCustomObject> router = factory.getRouter();
        MyCustomObject result = router.route( input );
    }
}
```

## Sample graph
```dot
digraph test {
	Start[shape=circle,fillcolor=green,style=filled];
	End[shape=circle,fillcolor=red,style=filled];
	GatewayCheckExit[shape=diamond,label="Check id",style=solid];

	Start -> GatewayCheckExit;
	GatewayCheckExit -> CreatePojoMessageResult  [if="input.id<0", label="Exit if id<0"];
	CreatePojoMessageResult -> End;

	GatewayCheckExit -> End [if="input.id>=0", label="End if id>=0"]
}
```

## Naming conventions for nodes
Node names shall be unique in the process to be able to correctly process paths in the graph.
When same type of node shall be used multiple times, 
the second node shall be named with different name adding **type** attribute.
Implicit naming conventions for node names is introduced for easier guessing for the node type. 

The **type** attribute can be specified for the node to explicitly specify type of node. 
If no type is specified and name of the node is not defined in processor configuration, 
then the following rules are applied:
 * Start - assumed *Start* task
 * End - names starting with 'End' are assumed *End* task
 * Gateway - names starting with 'Gateway' are assumed *Gateway* task
 * Script - names starting with 'Script' are assumed as *Script* task
 * GraphRoute - names starting with 'GraphRoute' are assumed *GraphRoute* task
 * Join - names starting with 'Join' are assumed *Join* task
 * Split - names starting with 'Split' are assumed *Split* task
 * Exit - names starting with 'Exit' are assumed *Exit* task

## Specific node attributes
Following types of nodes (task) have specific attributes:
 * Gateway - gateway task shall have **if** attributes, which will be evaluated in runtime to check for path in graph. *if* is groovy expression.
 * GraphRoute - graph route task shall have **process** attribute to specify name of the subprocess to route to.
 * Script - engine looks for **expr** attribute to execute with groovy interpreter. 
 * custom attributes - each node can have user-defined attributes (parameters). 
   These parameters can be used in task implementation with **getParameter(paramName)** method.
   The value of parameter can be literal or groovy expression. 
   Expressions shall be in format ${expression}

## Using process variables
Micrograph engine sets some implicit variables during execution. 
The scope of variables is the lifecycle of process. 
Additional variables can be set by the user as well. The implicit variables are:
 * input - this variable hold the process input object
 * result - this variable is hold the result object of processing. 
   It can be by tasks with **getProcess().setResult(resultObject)**
 * currTask - instance if current task object
 * appName - the name of the application
 * process - hold process instance
 * processId - the unique ID of the process. Each process execution has its unique ID;  
 * custom variables - any task can set custom variables with **setVariable(varName, varValue)**

## API details
* Process - custom processes can be attached to specific graph, using *processClass* in *process* section. The customs process class shall implement *Process* interface (or extend DefaultProcess). One can overide the following methods:
    * onEntry - called when process is started
    * onExit - called when process is finished (normal without exception)
    * onException - called when unhandled Exception is thrown during process execution
* Task - custom task shall implement *Task* (or extend *BaseTask*). One cam override the following methods:
    * execute - this method **must** be implemented with the specific processing logic
    * onEntry - called on task entry (before *execute* method is called)
    * onExit - called on task exit (after *execute* method finishes)

## Advanced - tips and tricks
 * Readability - for better readability specify labels and shapes in the nodes. Examples:
```dot
digraph NiceLookingGraph {
    GatewayCheckNull[shape=diamond,label="Check if input is null"];
    End[shape=circle,fillcolor=red,style=filled];
    Start[shape=circle,fillcolor=green,style=filled];

    Start -> GatewayCheckNull ;
    GatewayCheckNull -> NextTask [if="input==null",label="Null"];
    GatewayCheckNull -> End [if="input==null",label="Not null"];
    {NextTask[label="Do it now"]} -> End;
}
```
 * Conditional tasks - sometimes we want to execute a specific task only if some condition is met. 
   We can use **Gateway** task of course for better readability, 
   but in some cases we just want to skip execution of the task without changing execution path. 
   Then we can use **when** attribute to specify condition. 
   The task will be executed only if condition evaluates to true, 
   but the next task will be the node following current node. 
   The above graph for example can be written without *Gateway* in short syntax, thou if is not obvious that NextTask will not always be executed. 
   So use this feature with caution.
```dot
digraph ShorterGraph {
    End[shape=circle,fillcolor=red,style=filled];
    Start[shape=circle,fillcolor=green,style=filled];

    Start 
     -> {NextTask[label="Do this only if input is null",when="input==null"] } 
     -> End;
}
```

 * Sub-graph parameters - when routing from one graph to subgraph, you can pass parameters for destination graph. 
   They can be used in subgraph for expression (conditional) like *${process.parameters.paramName}. 
   Current process parameters are saved before entering in subgraph and restored when returned back. 
```dot
digraph GraphParams {
    End[shape=circle,fillcolor=red,style=filled];
    Start[shape=circle,fillcolor=green,style=filled];

    Start -> GraphRoute;
    GraphRoute -> End [process=SubGraphName, param1="useMe" ];
}
digraph SubGraph {
    End[shape=circle,fillcolor=red,style=filled];
    Start[shape=circle,fillcolor=green,style=filled];

    Start 
    -> {TaskUseMe[if="${process.parameters.param1=='useMe'}"]}
    -> {TaskDontUseMe[if="${process.parameters.param1!='useMe'}"]}
    -> End;
}
```